package cn.tenmg.flink.jobs.quickstart;

import com.alibaba.fastjson.JSON;

import cn.tenmg.dsl.utils.ClassUtils;
import cn.tenmg.flink.jobs.FlinkJobsPortal;
import cn.tenmg.flink.jobs.config.loader.XMLConfigLoader;
import cn.tenmg.flink.jobs.config.model.FlinkJobs;;

/**
 * test_table数据同步测试
 * 
 * @author June wjzhao@aliyun.com
 *
 */
public class TestTableTest {
	public static void main(String[] args) throws Exception {
		FlinkJobs flinkJobs = XMLConfigLoader.getInstance()
				.load(ClassUtils.getDefaultClassLoader().getResourceAsStream("TestTableTest.xml"));// 加载任务配置文件
		FlinkJobsPortal.main(JSON.toJSONString(flinkJobs, true));// 本地执行任务
	}
}
