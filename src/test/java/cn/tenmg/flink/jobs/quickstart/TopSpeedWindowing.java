package cn.tenmg.flink.jobs.quickstart;

import org.apache.flink.api.common.JobID;
import org.apache.flink.client.deployment.StandaloneClusterId;
import org.apache.flink.client.program.rest.RestClusterClient;

import cn.tenmg.dsl.utils.ClassUtils;
import cn.tenmg.flink.jobs.FlinkJobsClient;
import cn.tenmg.flink.jobs.clients.StandaloneRestClusterClient;
import cn.tenmg.flink.jobs.config.loader.XMLConfigLoader;
import cn.tenmg.flink.jobs.config.model.FlinkJobs;

public class TopSpeedWindowing {

	private static final FlinkJobsClient<RestClusterClient<StandaloneClusterId>> client = new StandaloneRestClusterClient();

	public static void main(String[] rags) throws Exception {
		FlinkJobs flinkJobs = XMLConfigLoader.getInstance()
				.load(ClassUtils.getDefaultClassLoader().getResourceAsStream("StarRocksTest.xml"));// 解析XML配置
		JobID jobid = client.submit(flinkJobs);// 提交到远程服务器集群执行任务
		Thread.sleep(80000);// 让子弹飞一会儿
		System.out.println("Savepoint: " + client.stop(jobid));// 尝试通过client停止远程任务
	}
}
