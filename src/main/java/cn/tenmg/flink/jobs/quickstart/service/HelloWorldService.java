package cn.tenmg.flink.jobs.quickstart.service;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import cn.tenmg.flink.jobs.StreamService;
import cn.tenmg.flink.jobs.model.Arguments;

public class HelloWorldService implements StreamService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1319210358735745176L;

	@Override
	public void run(StreamExecutionEnvironment env, Arguments arguments) throws Exception {
		DataStream<String> stream = env.fromElements("Hello, World!");
		stream.print();
	}

}
