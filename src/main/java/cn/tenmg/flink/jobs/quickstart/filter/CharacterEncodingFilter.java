package cn.tenmg.flink.jobs.quickstart.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 编码处理过滤器
 * 
 * @author June wjzhao@aliyun.com
 *
 */
public class CharacterEncodingFilter implements Filter {

	private String encoding = "utf-8";

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public CharacterEncodingFilter() {
		super();
	}

	public CharacterEncodingFilter(String encoding) {
		super();
		this.encoding = encoding;
	}

	public void init(FilterConfig filterConfig) {
		String encoding = filterConfig.getInitParameter("encoding");
		if (encoding != null) {
			this.encoding = encoding;
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);
		chain.doFilter(request, response);
	}

}
