package cn.tenmg.flink.jobs.quickstart;

import cn.tenmg.flink.jobs.FlinkJobsRunner;
import cn.tenmg.flink.jobs.StreamService;

/**
 * 应用入口类
 * 
 * @author 赵伟均 wjzhao@aliyun.com
 *
 */
public class App {

	/**
	 * 基础包名
	 */
	private static final String basePackage = "cn.tenmg.flink.jobs.quickstart.service";

	public static void main(String[] args) throws Exception {
		FlinkJobsRunner runner = new FlinkJobsRunner() {

			@SuppressWarnings("unchecked")
			@Override
			protected StreamService getStreamService(String serviceName) {// 根据类名获取流服务实例
				StreamService streamService = null;
				try {
					Class<StreamService> streamServiceClass = (Class<StreamService>) Class
							.forName(basePackage + "." + serviceName);
					streamService = streamServiceClass.newInstance();
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
				return streamService;
			}

		};
		runner.run(args);
	}

}
